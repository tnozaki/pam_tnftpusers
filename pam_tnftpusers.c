/*-
 * Copyright (c)2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include <errno.h>
#include <paths.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

#include "tnftpusers.h"

#define PAM_SM_ACCOUNT
#define PAM_SM_SESSION

#include <security/pam_appl.h>
#include <security/pam_modules.h>
#include <security/pam_mod_misc.h>
#include <security/openpam.h>

#define _PATH_FTPCHROOT	"/etc/ftpchroot"

#define TNFTPUSERS_FTPUSERS	"ftpusers"
#define TNFTPUSERS_FTPCHROOT	"ftpchroot"

static void
/*ARGSUSED*/
class_delete(pam_handle_t *pamh, void *data, int pam_end_status)
{
	free(data);
}

static inline void
param_init(pam_handle_t *pamh, struct tnftpd_param *param)
{
	param->ftpusers = openpam_get_option(pamh, TNFTPUSERS_FTPUSERS);
	if (param->ftpusers == NULL)
		param->ftpusers = _PATH_FTPUSERS;
	param->ftpchroot = openpam_get_option(pamh, TNFTPUSERS_FTPCHROOT);
	if (param->ftpchroot == NULL)
		param->ftpchroot = _PATH_FTPCHROOT;
}

PAM_EXTERN int
/*ARGSUSED*/
pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc, const char *argv[])
{
	int pam_err;
	const char *user, *rhost;
	char *class;
	struct tnftpd_param param;

	pam_err = pam_get_user(pamh, &user, NULL);
	if (pam_err != PAM_SUCCESS)
		return pam_err;
	if (user == NULL)
		return PAM_SERVICE_ERR;
	pam_err = pam_get_item(pamh, PAM_RHOST, (const void **)&rhost);
	if (pam_err != PAM_SUCCESS)
		rhost = NULL;
	param_init(pamh, &param);

	pam_err = PAM_AUTH_ERR;
	if (!tnftpd_check_account(&param, user, rhost, &class)) {
		if (class == NULL) {
			pam_err = PAM_SUCCESS;
		} else {
			pam_err = pam_set_data(pamh,
			    TNFTPUSERS_CLASS, class, &class_delete);
			if (pam_err != PAM_SUCCESS)
				free(class);
		}
	}
	return pam_err;
}

PAM_EXTERN int
/*ARGSUSED*/
pam_sm_open_session(pam_handle_t *pamh, int flags, int argc, const char *argv[])
{
	int pam_err, ret;
	const char *user, *rhost, *cwd;
	struct tnftpd_param param;
	char *homedir;

	pam_err = pam_get_user(pamh, &user, NULL);
	if (pam_err != PAM_SUCCESS)
		return pam_err;
	if (user == NULL)
		return PAM_SERVICE_ERR;
	pam_err = pam_get_item(pamh, PAM_RHOST, (const void **)&rhost);
	if (pam_err != PAM_SUCCESS)
		rhost = NULL;
	param_init(pamh, &param);

	pam_err = PAM_SESSION_ERR;
	homedir = tnftpd_check_chroot(&param, user, rhost, &ret);
	if (homedir != NULL) {
		if (!ret) {
			if (chroot(homedir) == -1)
				goto fatal;
			cwd = "/";
		} else {
			cwd = homedir;
		}
		if (chdir(cwd) != -1)
			pam_err = pam_setenv(pamh, "HOME", cwd, 1);
fatal:
		free(homedir);
        }
	return pam_err;
}

PAM_EXTERN int
/*ARGSUSED*/
pam_sm_close_session(pam_handle_t *pamh, int flags, int argc, const char *argv[])
{
	return PAM_SUCCESS;
}

PAM_MODULE_ENTRY("pam_tnftpusers");
