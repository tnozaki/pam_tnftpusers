/*-
 * Copyright (c)2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <errno.h>
#include <fcntl.h>
#include <fnmatch.h>
#include <grp.h>
#include <limits.h>
#include <netdb.h>
#include <pwd.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "pcs.h"
#include "tnftpusers.h"

struct userinfo {
	struct passwd *pwd, pwdbuf;
	char *buf;
	size_t bufsiz;
	gid_t *groups;
	int ngroups;
	char *rhost;
	struct addrinfo *raddr;
};

struct ftpusers {
	struct userinfo userinfo;
	char **class;
};

static inline char *
skip_blank(char *s)
{
	while (pcs_isblank((unsigned char)*s))
		++s;
	return s;
}

static inline char*
skip_nonblank(char *s)
{
	while (*s && !pcs_isblank((unsigned char)*s))
		++s;
	return s;
}

static inline char *
line_tokenize(char **line)
{
	char *head, *tail;

	head = skip_blank(*line);
	tail = skip_nonblank(head);
	if (*tail)
		*tail++ = '\0';
	*line = tail;
	return head;
}

static inline char *
sfparseln(FILE *fp)
{
	return fparseln(fp, NULL, NULL, NULL,
	    FPARSELN_UNESCALL & ~FPARSELN_UNESCREST);
}

static inline struct passwd *
sgetpwnam_r(const char *name, struct passwd *pwd,
    char **buf, size_t *siz)
{
	long val;
	char *p;
	struct passwd *ret;

	if (*buf == NULL) {
		val = sysconf(_SC_GETPW_R_SIZE_MAX);
		if (val < 0)
			return NULL;
		*siz = (size_t)val;
		*buf = malloc(*siz);
		if (*buf == NULL)
			return NULL;
	}
	for (;;) {
		if (!getpwnam_r(name, pwd, *buf, *siz, &ret))
			return ret;
		if (errno != ERANGE)
			break;
		if (SIZE_MAX / 2 < *siz)
			break;
		*siz *= 2;
		if ((p = realloc(*buf, *siz)) == NULL)
			break;
		*buf = p;
	}
	return NULL;
}

static inline int
sgetgroupmembership(struct passwd *pwd, gid_t **groups)
{
	gid_t *p, *q;
	int siz, ngroups;

	p = NULL;
	siz = 16;
	for (p = NULL, siz = 16;
	    (q = realloc(p, siz * sizeof(*groups))) != NULL; siz *= 2) {
		p = q;
		if (!getgroupmembership(pwd->pw_name, pwd->pw_gid,
		    p, siz, &ngroups)) {
			*groups = p;
			return ngroups;
		}
		if (INT_MAX / 2 < siz)
			break;
	}
	free(p);
	return -1;
}

static inline struct group *
sgetgrgid_r(gid_t gid, struct group *grp, char **buf, size_t *siz)
{
	long val;
	char *p;
	struct group *ret;

	if (*buf == NULL) {
		val = sysconf(_SC_GETGR_R_SIZE_MAX);
		if (val < 0)
			return NULL;
		*siz = (size_t)val;
		*buf = malloc(*siz);
		if (*buf == NULL)
			return NULL;
	}
	for (;;) {
		if (!getgrgid_r(gid, grp, *buf, *siz, &ret))
			return ret;
		if (errno != ERANGE)
			break;
		if (SIZE_MAX / 2 < *siz)
			break;
		*siz *= 2;
		if ((p = realloc(*buf, *siz)) == NULL)
			break;
		*buf = p;
	}
	return NULL;
}

static inline void
bits_netmask4(int bits, struct in_addr *mask)
{
	mask->s_addr = htonl(0xFFFFFFFFUL << (32 - bits));
}

static inline int
cidr_match4(struct in_addr *addr, struct in_addr *net, struct in_addr *mask)
{
	if ((addr->s_addr & mask->s_addr) != net->s_addr)
		return 0;
	return 1;
}

static inline void
bits_netmask6(int bits, struct in6_addr *mask)
{
	int i;

	i = 0;
	while (i < bits / 8)
		mask->s6_addr[i++] = 0xFF;
	if (i < 16)
		mask->s6_addr[i++] = 0xFF << (8 - (bits % 8));
	while (i < 16)
		mask->s6_addr[i++] = 0;
}

static inline int
cidr_match6(struct in6_addr *addr, struct in6_addr *net, struct in6_addr *mask)
{
	int i;

	for (i = 0; i < 16; i++)
		if ((addr->s6_addr[i] & mask->s6_addr[i]) != net->s6_addr[i])
			return 0;
	return 1;
}

static inline int
userinfo_init(struct userinfo *u, const char *user, const char *rhost)
{
	struct addrinfo hint;

	u->buf = NULL;
	u->pwd = sgetpwnam_r(user, &u->pwdbuf, &u->buf, &u->bufsiz);
	if (u->pwd != NULL) {
		u->ngroups = sgetgroupmembership(u->pwd, &u->groups);
		if (u->ngroups != -1) {
			if (rhost == NULL) {
				u->rhost = NULL;
				u->raddr = NULL;
				return 0;
			}
			u->rhost = strdup(rhost);
			if (u->rhost != NULL) {
				memset(&hint, 0, sizeof(hint));
				hint.ai_family = AF_UNSPEC;
				hint.ai_socktype = SOCK_STREAM;
				if (!getaddrinfo(u->rhost, NULL,
				    &hint, &u->raddr))
					return 0;
				free(u->rhost);
			}
			free(u->groups);
		}
	}
	free(u->buf);
	return 1;
}

static inline void
userinfo_uninit(struct userinfo *u)
{
	free(u->buf);
	free(u->groups);
	free(u->rhost);
	if (u->raddr != NULL)
		freeaddrinfo(u->raddr);
}

static inline int
check_user(struct userinfo *u, char *token)
{
	struct passwd *pwd;

	pwd = u->pwd;
	if (!fnmatch(token, pwd->pw_name, 0))
		return 0;
	return 1;
}

static inline int
check_group(struct userinfo *u, char *token)
{
	int ret, i;
	struct group *grp, grpbuf;
	char *buf;
	size_t bufsiz;

	ret = 1;
	buf = NULL;
	for (i = 0; i < u->ngroups; ++i) {
		grp = sgetgrgid_r(u->groups[i], &grpbuf, &buf, &bufsiz);
		if (!fnmatch(token, grp->gr_name, 0)) {
			ret = 0;
			break;
		}
	}
	free(buf);
	return ret;
}

static inline int
check_raddr4(struct userinfo *u, char *token)
{
	int bits;
	struct in_addr addr, mask;
	struct addrinfo *ai;
	struct sockaddr_in *sai;

	memset(&addr, 0, sizeof(addr));
	if ((bits = inet_net_pton(AF_INET, token,
	    (char *)&addr, sizeof(addr))) > 0) {
		bits_netmask4(bits, &mask);
		for (ai = u->raddr; ai != NULL; ai = ai->ai_next) {
			if (ai->ai_family == AF_INET) {
				sai = (struct sockaddr_in *)ai->ai_addr;
				if (cidr_match4(&addr, &sai->sin_addr, &mask))
					return 0;
			}
		}
	}
	return 1;
}

static inline int
check_raddr6(struct userinfo *u, char *token)
{
	int bits;
	struct in6_addr addr, mask;
	struct addrinfo *ai;
	struct sockaddr_in6 *sai;

	memset(&addr, 0, sizeof(addr));
	if ((bits = inet_net_pton(AF_INET6, token,
	    (char *)&addr, sizeof(addr))) > 0) {
		bits_netmask6(bits, &mask);
		for (ai = u->raddr; ai != NULL; ai = ai->ai_next) {
			if (ai->ai_family == AF_INET6) {
				sai = (struct sockaddr_in6 *)ai->ai_addr;
				if (cidr_match6(&addr, &sai->sin6_addr, &mask))
					return 0;
			}
		}
	}
	return 1;
}

static inline int
check_rhost(struct userinfo *u, char *token)
{
	if (u->rhost && !fnmatch(token, u->rhost, FNM_CASEFOLD))
                return 0;
	return 1;
}

static inline int
check_word(struct userinfo *u, char *token)
{
	char *s;
	int delim;

	/* user */
	s = token;
	while (*token && *token != ':' && *token != '@')
		++token;
	delim = *token;
	if (delim)
		*token++ = '\0';
	if (*s && check_user(u, s))
		return 1;
	switch (delim) {
	case ':':
		/* group */
		s = token;
		while (*token && *token != '@')
			++token;
		if (*token)
			*token++ = '\0';
		if (*s && check_group(u, s))
			return 1;
	/*FALLTHROUGH*/
	case '@':
		/* host */
		if (*token) {
			if (check_raddr4(u, token) &&
			    check_raddr6(u, token) &&
			    check_rhost(u, token))
				return 1;
		}
	}
	return 0;
}

static inline int
/*ARGSUSED*/
check_perm(struct userinfo *u, char *token)
{
	if (!pcs_strcasecmp(token, "allow") || !pcs_strcasecmp(token, "yes"))
		return 0;
	return 1;
}

static inline int
/*ARGSUSED*/
check_class(struct userinfo *u, char *token)
{
	if (pcs_strcasecmp(token, "all") && pcs_strcasecmp(token, "none"))
		return 0;
	return 1;
}

static inline FILE *
open_file(const char *path)
{
	int fd;
	struct stat st;
	FILE *fp;

	if ((fd = open(path, O_RDONLY|O_CLOEXEC)) != -1) {
		if (fstat(fd, &st) != -1 && S_ISREG(st.st_mode)) {
			fp = fdopen(fd, "re");
			if (fp != NULL)
				return fp;
		}
		close(fd);
	}
	return NULL;
}

static inline int
parse_file(const char *file, void *ctx, int norecord,
    int (*handler)(void *, char *, char *, char *, int *))
{
	FILE *fp;
	int ret;
	char *line, *t, *arg1, *arg2, *arg3;

	fp = open_file(file);
	if (fp == NULL)
		return -1;

	ret = norecord;
	for (; (line = sfparseln(fp)) != NULL; free(line)) {
		t = line;
		arg1 = line_tokenize(&t);
		arg2 = line_tokenize(&t);
		arg3 = line_tokenize(&t);
		if (!(*handler)(ctx, arg1, arg2, arg3, &ret))
			break;
	}
	free(line);
	if (ferror(fp))
		ret = -1;
	if (fclose(fp))
		ret = -1;
	return ret;
}

static int
parse_ftpusers_handler(void *ctx, char *arg1, char *arg2, char *arg3, int *result)
{
	struct ftpusers *p;
	struct userinfo *u;
	int ret;

	p = (struct ftpusers *)ctx;
	u = &p->userinfo;
	if (*arg1 && !check_word(u, arg1)) {
		ret = 1;
		if (*arg2 && !check_perm(u, arg2))
			ret = 0;
		if (*arg3 && !check_class(u, arg3)) {
			*p->class = strdup(arg3);
			if (*p->class == NULL)
				ret = -1;
		}
		*result = ret;
		return 0;
	}
	return 1;
}

static inline int
parse_ftpusers(const char *file, struct ftpusers *p)
{
        return parse_file(file, p, 1, &parse_ftpusers_handler);
}

static int
parse_ftpchroot_handler(void *ctx, char *arg1, char *arg2, char *arg3, int *result)
{
	struct userinfo *u;
	int ret;

	u = (struct userinfo *)ctx;
	if (*arg1 && !check_word(u, arg1)) {
		ret = 1;
		if (!*arg2 || !check_perm(u, arg1))
			ret = 0;
		*result = ret;
		return 0;
	}
	return 1;
}

static inline int
parse_ftpchroot(const char *file, struct userinfo *u)
{
	return parse_file(file, u, 1, &parse_ftpchroot_handler);
}

int
tnftpd_check_account(struct tnftpd_param *param,
    const char *user, const char *rhost, char **class)
{
	int ret;
	struct ftpusers p;

	ret = 1;
	if (!userinfo_init(&p.userinfo, user, rhost)) {
		p.class = class;
		ret = parse_ftpusers(param->ftpusers, &p);
		userinfo_uninit(&p.userinfo);
	}
	return ret;
}

char *
tnftpd_check_chroot(struct tnftpd_param *param,
    const char *user, const char *rhost, int *ret)
{
	struct userinfo u;
	struct passwd *pwd;
	char *homedir;

	homedir = NULL;
	if (!userinfo_init(&u, user, rhost)) {
		pwd = u.pwd;
		homedir = strdup(pwd->pw_dir);
		if (homedir != NULL)
			*ret = parse_ftpchroot(param->ftpchroot, &u);
		userinfo_uninit(&u);
	}
	return homedir;
}
