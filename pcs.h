/*-
 * Copyright (c)2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
#ifndef PCS_H_
#define PCS_H_

static inline int
pcs_isupper(int c)
{
	return c >= 'A' && c <= 'Z';
}

static inline int
pcs_islower(int c)
{
	return c >= 'a' && c <= 'z';
}

static inline int
pcs_isblank(int c)
{
	return c == ' ' || c == '\t';
}

static inline int
pcs_toupper(int c)
{
	return pcs_islower(c) ? (c - 'a' + 'A') : c;
}

static inline int
pcs_tolower(int c)
{
	return pcs_isupper(c) ? (c - 'A' + 'a') : c;
}

static inline int
pcs_strcasecmp(const char *s1, const char *s2)
{
	while (pcs_tolower((unsigned char)*s1) ==
	       pcs_tolower((unsigned char)*s2)) {
		if (!*s1)
			return 0;
		++s1, ++s2;
	}
	return pcs_tolower((unsigned char)*s1) -
	       pcs_tolower((unsigned char)*s2);
}

#endif /*PCS_H_*/
